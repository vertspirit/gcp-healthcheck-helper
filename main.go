package main

import (
  "log"
  "fmt"

  "gcp-healthcheck-helper/pkg/socket-server"
)

var (
  Version  string
  Build    string
)

func main() {
  fmt.Println("[gcp-healthcheck-helper] Version:", Version)
  fmt.Println("[gcp-healthcheck-helper] Build Date:", Build)

  L := &socket_server.SocketServer {
    Protocol: protoParam,
  }
  err := L.Listen(hostParam, portParam)
  if err != nil {
    log.Fatal(err)
  }
  defer L.Stop()

  var defaultCoroutines int
  if !(Settings.Coroutines > 0) {
    defaultCoroutines = 2
  } else {
    defaultCoroutines = Settings.Coroutines
  }

  if selfcheckParam {
    fmt.Println("[gcp-healthcheck-helper] Enabled the self-check mode")
    L.StartSelfCheck(Settings.WhiteList, timeoutParam, Settings.VirtualIP, Settings.Response, "ipv4", Settings.Interface, Settings.Excluded)
  } else {
    L.Start(Settings.WhiteList, timeoutParam, Settings.Response, "ipv4", Settings.Interface, Settings.Excluded, defaultCoroutines)
  }

}

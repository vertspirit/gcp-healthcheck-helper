# GCP Healthcheck Helper

A simple tool to check the virtual ip address on the host or not, this could help the gcp tcp/udp loader balancer to find the primary node of linux cluster.

## Build and installation

Run the scripts in the build/ directory to build deb or rpm package via fpm:

```bash
sh build/build-package-deb.sh
```

You will get the deb or rpm file in the build/ directory when the script finished, and run the apt or yum to install the package:

```bash
sudo apt-get install ./build/gcp-healthcheck-helper-*.deb
```

## Usage

Install this package on all nodes, and edit the /etc/gcp-healthcheck-helper/config.yaml file to modify the vip options to the vip target to check:

```
vip: "10.30.0.60"
```

And restart the gcp-healthcheck-helper daemon to apply change:

```bash
sudo systemctl restart gcp-healthcheck-helper.service
```

Set the request option of gcp health check as "10.30.0.60" (or something else), and the reponse option to "1".
Then the node of internal tcp/udp load balancer wihch carry the vip will be healthy (only one healthy node in the linux cluster).


package main

import (
  "testing"
  "sync"

  "gcp-healthcheck-helper/pkg/socket-server"
)

type socketServer = socket_server.SocketServer
type tcpClient = socket_server.TcpClient

func TestTCPCheckVIP(t *testing.T) {

  testHost := "127.0.0.1"
  testPort := 59995
  testL := &socketServer {
    Protocol: "tcp",
    UnitTest: true,
  }

  err := testL.Listen(testHost, testPort)
  if err != nil {
    t.Log("The error should be nil.")
    t.Fail()
  }
  defer testL.Stop()

  whiteList := make([]string, 0)
  timeout := 5
  response := "1"
  iface := "lo"
  excluded := []string {"docker"}
  coroutines := 2 // not working for tcp protocol

  wg := sync.WaitGroup{}
  // hit
  wg.Add(1)
  go func() {
    defer wg.Done()
    testClientHit := &tcpClient {
      Host: testHost,
      Port: testPort,
      Request: "127.0.0.1",
      Timeout: timeout,
    }
    result, err := testClientHit.Send()
    if err != nil || result != "1" {
      t.Log("The error should be nil. And the result should be 1 (hit).")
      t.Fail()
    }
  }()
  testL.Start(whiteList, timeout, response, "ipv4", iface, excluded, coroutines)

  // miss
  wg.Add(1)
  go func() {
    defer wg.Done()
    testClientMiss := &tcpClient {
      Host: testHost,
      Port: testPort,
      Request: "192.168.0.0",
      Timeout: timeout,
    }
    result, err := testClientMiss.Send()
    if err != nil || result != "0" {
      t.Log("The error should be nil. And the result should be 0 (miss).")
      t.Fail()
    }
  }()
  testL.Start(whiteList, timeout, response, "ipv4", iface, excluded, coroutines)

  wg.Wait()
}

// Usage:  go test -test.run TestTCPCheckVIP -config config.yaml

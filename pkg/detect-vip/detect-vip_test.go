package detect_vip

import (
  "testing"
)

func TestCheckVIP(t *testing.T) {
  failed := false
  excluded := make([]string)
  ifName := "lo"
  list := detect_vip.GetInterfacesAddrs("ipv4", ifName, excluded)
  // Check the vip should exist
  vip := "127.0.0.1"
  if ! CheckExists(list, vip) {
    t.Log("The vip should exist! The result is not correct.")
    failed = true
  }
  // Check the vip should not exist
  vipNot := "192.168.0.1"
  if CheckExists(list, vipNot) {
    t.Log("The vip should NOT exist! The result is not correct.")
    failed = true
  }

  if failed {
    t.Fail()
  }
}

// Usage:  go test -test.run TestCheckVIP

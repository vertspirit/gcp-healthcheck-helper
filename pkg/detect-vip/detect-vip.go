package detect_vip

import (
  "net"
  "log"
  "regexp"
  "sync"
)

func CheckExists(list []string, val string) bool {
  for _, item := range list {
    if item == val {
      return true
    }
  }
  return false
}

func CheckExistsRegex(list []string, val string) bool {
  for _, item := range list {
    matched, _ := regexp.MatchString(item + ".*", val)
    if matched {
      return true
    }
  }
  return false
}

func GetInterfacesAddrs(ipVer, ifName string, excluded []string) []string {
  // ipVer could be ipv4 or ipv6
  var err error
  var ipList []string
  var ifList []net.Interface
  ch := make(chan string, 16)
  done := make(chan int)
  wg := sync.WaitGroup{}

  if ifName != "all" {
    iface, err := net.InterfaceByName(ifName)
    if err != nil {
      log.Println(err)
    } else {
      ifList = append(ifList, *iface)
    }
  } else {
    ifList, err = net.Interfaces()
    if err != nil {
      log.Println(err)
    }
  }

  if len(ifList) == 0 {
    return ipList
  }

  for _, iface := range ifList {
    // fmt.Println(iface.Name, iface.HardwareAddr, iface.MTU)
    wg.Add(1)
    go func(ch chan <- string, iface net.Interface) {
      defer wg.Done()
      var addrList []net.Addr
      if ! CheckExistsRegex(excluded, iface.Name) {
        addrList, err = iface.Addrs()
        if err != nil {
          log.Println(err)
        }

        for _, addr := range addrList {
          switch ip := addr.(type) {
          case *net.IPNet:
            // fmt.Println("type *net.IPNet (ipv4):", ip.IP, "Mask:", ip.Mask)
            check := ip.IP.To4()
            if ipVer != "ipv6" && check != nil {
              ch <- ip.IP.String()
            } else if ipVer == "ipv6" && check == nil {
              ch <- ip.IP.String()
            }
          case *net.IPAddr:
            // fmt.Println("type *net.IPAddr (ipv6):", ip.IP, "Zone:", ip.Zone)
            if ipVer == "ipv6" {
              ch <- ip.IP.String()
            }
          }
        }
      }
    }(ch, iface)

    go func() {
      for {
        select {
        case val, ok := <-ch:
          if !ok{
            done <- 1
            break
          } else {
            ipList = append(ipList, val)
          }
        default:
        }
      }
    }()
  }
  wg.Wait()
  close(ch)
  <-done
  return ipList
}

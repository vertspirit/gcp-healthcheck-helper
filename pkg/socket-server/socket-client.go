package socket_server

import(
  "net"
  "unsafe"
  "strconv"
  "time"
  "bufio"
)

type TcpClient struct {
  Host     string
  Request  string
  Port     int
  Timeout  int
}

func (client *TcpClient) Send() (string, error) {
  serverAddr :=  client.Host + ":" + strconv.Itoa(client.Port)
  dialer := net.Dialer{ Timeout: time.Duration(client.Timeout) * time.Second }
  conn, err := dialer.Dial("tcp", serverAddr)
  if err != nil {
    return "error for dial", err
  }
  defer conn.Close()

  _, err = conn.Write([]byte(client.Request + "\n"))
  if err != nil {
    return "error for request", err
  }

  var response []byte
  response, _, err = bufio.NewReader(conn).ReadLine()
  if err != nil {
    return "error for read", err
  }

  return *(*string)(unsafe.Pointer(&response)), err
}

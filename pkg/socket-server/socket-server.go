package socket_server

import (
  "net"
  "fmt"
  "log"
  "bufio"
  "io"
  "bytes"
  "time"
  "strings"
  "strconv"

  "gcp-healthcheck-helper/pkg/detect-vip"
)

const AsciiNewline string = "\r\n" // ASCII newline is CRLF

func CheckIP(addr string) bool {
  check := net.ParseIP(addr)
  if check.To4() == nil {
    return false
  } else {
    return true
  }
}

func IsInCIDR (addr, cidr string) bool {
  targetIP := net.ParseIP(addr)
  _, ipRange, err := net.ParseCIDR(cidr)
  if err != nil {
    return false
  }
  if targetIP.To4() == nil {
    return false
  }
  if ipRange.Contains(targetIP) {
    return true
  } else {
    return false
  }
}

type SocketServer struct {
  Listener      net.Listener
  UDPConn       *net.UDPConn
  Protocol      string
  UnitTest      bool
}

func (l *SocketServer) Listen(ip string, port int) error {
  var err error
  switch l.Protocol {
  case "tcp":
    addr := net.ParseIP(ip).String() + ":" + strconv.Itoa(port)
    listener, err := net.Listen("tcp", addr)
    if err == nil {
      l.Listener = listener
      fmt.Printf("[gcp-healthcheck-helper] +++ Listening on %v +++\n", addr)
      fmt.Printf("[gcp-healthcheck-helper] +++ Protocol: %s +++\n", l.Protocol)
    } else {
      log.Println("ERROR:", err)
      log.Fatalf("--- Fail to listen on %v ---", addr)
    }
  case "udp":
    addr := &net.UDPAddr{
        Port: port,
        IP:   net.ParseIP(ip),
    }
    conn, err := net.ListenUDP("udp", addr)
    if err == nil {
      l.UDPConn = conn
      fmt.Printf("[gcp-healthcheck-helper] +++ Listening on %v +++\n", addr.IP.String() + ":" + strconv.Itoa(addr.Port))
      fmt.Printf("[gcp-healthcheck-helper] +++ Protocol: %s +++\n", l.Protocol)
    } else {
      log.Println("ERROR:", err)
      log.Fatalf("--- Fail to listen on %v ---", addr)
    }
  }
  return err
}

func (l *SocketServer) Stop() {
  if l.Protocol == "tcp" {
    l.Listener.Close()
  }
}

func (l *SocketServer) Start(whiteList []string, timeout int, resp, ipv, iface string, excluded []string, coroutines int) {
  switch l.Protocol {
  case "tcp":
    for {
      conn, err := l.Listener.Accept()
      conn.SetReadDeadline(time.Now().Add(time.Duration(timeout) * time.Second))
      conn.SetWriteDeadline(time.Now().Add(time.Duration(timeout) * time.Second))
      remoteAddr := conn.RemoteAddr().String() // get the remote ip address

      if len(whiteList) > 0 {
        blocked := false
        for _, cidr := range whiteList {
          if !IsInCIDR(remoteAddr, cidr) {
            blocked = true
            break
          }
        }
        if blocked {
          log.Println(">>> The blcoked ip which not in the white list:", remoteAddr)
          conn.Close()
          continue
        }
      }

      if err != nil {
        log.Println(err)
      } else if l.UnitTest {
        // for unit test
        tcpConnHandler(conn, resp, ipv, iface, excluded)
        break
      } else {
        go tcpConnHandler(conn, resp, ipv, iface, excluded)
      }
    }
  case "udp":
    var quit chan int
    for i := 0; i < coroutines; i++ {
      go udpConnHandler(l.UDPConn, quit, resp, ipv, iface, excluded, whiteList)
    }
    <-quit // blocking until error
  }
}

func (l *SocketServer) StartSelfCheck(whiteList []string, timeout int, vip, resp, ipv, iface string, excluded []string) {
  switch l.Protocol {
  case "tcp":
    for {
      conn, err := l.Listener.Accept()
      conn.SetReadDeadline(time.Now().Add(time.Duration(timeout) * time.Second))
      conn.SetWriteDeadline(time.Now().Add(time.Duration(timeout) * time.Second))
      remoteAddr := conn.RemoteAddr().String()
      if len(whiteList) > 0 {
        blocked := false
        for _, cidr := range whiteList {
          if !IsInCIDR(remoteAddr, cidr) {
            blocked = true
            break
          }
        }
        if blocked {
          log.Println(">>> The blcoked ip which not in the white list:", remoteAddr)
          conn.Close()
          continue
        }
      }
      if err != nil {
        log.Println(err)
      } else if l.UnitTest {
        tcpSelfCheck(conn, vip, resp, ipv, iface, excluded)
        break
      } else {
        go tcpSelfCheck(conn, vip, resp, ipv, iface, excluded)
      }
    }
  default:
    log.Fatal("The self-check mode only support tcp protocol.")
  }
}

func RequestReader(c *net.Conn) (string, error) {
  var buf bytes.Buffer
  r := bufio.NewReader(*c)
  message, err := r.ReadString('\n')
  switch err {
  case io.EOF:
    log.Println("tcp client colesd this connection.")
  case nil:
  default:
    log.Println(err)
  }
  buf.Write([]byte(message))
  return strings.TrimSpace(buf.String()), err
}

func ResponseWriter(c *net.Conn, message *string) error {
  w := bufio.NewWriter(*c)
  _, err := w.WriteString(*message)
  if err != nil {
    log.Println(err)
  } else {
    err = w.Flush()
  }
  // log.Println("Response bytes:", respBytes)
  return err
}

func tcpConnHandler(c net.Conn, r, ipv, iface string, excluded []string) {
  defer c.Close()
  var resp string

  addrs := detect_vip.GetInterfacesAddrs(ipv, iface, excluded)
  if len(addrs) == 0 {
    resp = "empty" + AsciiNewline
    err := ResponseWriter(&c, &resp)
    if err != nil {
      log.Println(err)
    }
    return
  }

  message, err := RequestReader(&c)
  if err != nil {
    log.Println(err)
  }

  check := detect_vip.CheckExists(addrs, message)
  if check {
    resp = r + AsciiNewline
  } else {
    resp = "0" + AsciiNewline
  }

  err = ResponseWriter(&c, &resp)
  if err != nil {
    log.Println(err)
  }
}

func udpConnHandler(uc *net.UDPConn, quit chan int, r, ipv, iface string, excluded, whiteList []string) {
  buffer := make([]byte, 1024)
  length, remoteAddr, err := 0, new(net.UDPAddr), error(nil)
  defer uc.Close()

  for err == nil {
    length, remoteAddr, err = uc.ReadFromUDP(buffer)
    recieved := strings.TrimSpace(string(buffer[:length]))
    log.Println("+++ Received packet from", remoteAddr.String(), "-", recieved)

    if len(whiteList) > 0 {
      blocked := false
      for _, cidr := range whiteList {
        if !IsInCIDR(remoteAddr.String(), cidr) {
          blocked = true
          break
        }
      }
      if blocked {
        log.Println(">>> The blcoked ip which not in the white list:", remoteAddr)
        continue
      }
    }

    addrs := detect_vip.GetInterfacesAddrs(ipv, iface, excluded)
    var resp []byte

    if len(addrs) == 0 {
      resp = []byte("empty" + AsciiNewline)
    } else {
      check := detect_vip.CheckExists(addrs, recieved)
      if check {
        resp = []byte(r + AsciiNewline)
      } else {
        resp = []byte("0" + AsciiNewline)
      }
    }

    uc.WriteToUDP(resp, remoteAddr)
    log.Println("--- Sent packet to:" + remoteAddr.String())
  }

  log.Println("The listener failed - ", err)
  quit <- 1
}

func tcpSelfCheck(c net.Conn, vip, r, ipv, iface string, excluded []string) {
  defer c.Close()
  var resp string

  addrs := detect_vip.GetInterfacesAddrs(ipv, iface, excluded)
  if len(addrs) == 0 {
    resp = "empty" + AsciiNewline
    err := ResponseWriter(&c, &resp)
    if err != nil {
      log.Println(err)
    }
    return
  }

  check := detect_vip.CheckExists(addrs, vip)
  if check {
    resp = r + AsciiNewline
  } else {
    resp = "0" + AsciiNewline
  }

  tmp := make([]byte, 1024)
  _, err := c.Read(tmp)
  switch err {
  case io.EOF:
    log.Println("tcp client colesd this connection.")
  case nil:
  default:
    log.Println(err)
  }

  err = ResponseWriter(&c, &resp)
  if err != nil {
    log.Println("Response Error:",err)
  }
}

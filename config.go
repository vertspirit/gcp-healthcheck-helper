package main

import (
  "log"
  "io/ioutil"

  "gopkg.in/yaml.v2"
)

var (
  Settings  *Config
)

type Config struct {
  WhiteList     []string   `yaml:"whitelist"`
  Excluded      []string   `yaml:"excluded"`
  VirtualIP     string     `yaml:"vip"`
  Interface     string     `yaml:"interface"`
  Response      string     `yaml:"response"`
  Protocol      string     `yaml:"protocol"`
  Coroutines    int        `yaml:"coroutines"`
}

func LoadYamlFile(configFile string) []byte {
  data, err := ioutil.ReadFile(configFile)
  if err != nil {
    log.Fatal(err)
  }
  return data
}

func ParseYaml(yamlData []byte) *Config {
  var conf *Config
  err := yaml.Unmarshal(yamlData, &conf)
  if err != nil {
    log.Fatal(err)
  }
  return conf
}

func init() {
  configYaml := LoadYamlFile(configParam)
  Settings = ParseYaml(configYaml)
}

package main

import (
  "flag"
  "log"
  "log/syslog"
  "testing"
)

var (
  hostParam       string
  configParam     string
  protoParam      string
  portParam       int
  timeoutParam    int
  syslogParam     bool
  whitelistParam  bool
  selfcheckParam  bool
)

func init() {
  // to fix the issue about 'golang 13 changed the order of the test initializer' that causes 'flag provided but not defined: -test.timeout'
  var _ = func() bool {
    testing.Init()
    return true
  }()

  flag.StringVar(&hostParam, "host", "0.0.0.0", "The ip address of socket server.")
  flag.StringVar(&hostParam, "h", "0.0.0.0", "The ip address of socket server. (shorten)")
  flag.IntVar(&portParam, "port", 59995, "The port of socket server.")
  flag.IntVar(&portParam, "p", 59995, "The port of socket server. (shorten)")
  flag.StringVar(&protoParam, "protocol", "tcp", "The protocol of socket server.")
  flag.StringVar(&protoParam, "l", "tcp", "The protocol of socket server. (shorten)")
  flag.StringVar(&configParam, "config", "/etc/gcp-healthcheck-helper/config.yaml", "The config file.")
  flag.StringVar(&configParam, "c", "config.yaml", "The config file. (shorten)")
  flag.IntVar(&timeoutParam, "timeout", 10, "The idle timeout the tcp client connection, default 2 seconds.")
  flag.IntVar(&timeoutParam, "t", 10, "The idle timeout the tcp client connection, default 5 seconds. (shorten)")
  flag.BoolVar(&syslogParam, "syslog", false, "Enbale the logging to the syslog in the system.")
  flag.BoolVar(&syslogParam, "s", false, "Enbale the logging to the syslog on the system. (shorten)")
  flag.BoolVar(&whitelistParam, "whitelist", false, "Enbale the whitelist function to block untrusted connections.")
  flag.BoolVar(&whitelistParam, "w", false, "Enbale the whitelist function to block untrusted connections. (shorten)")
  flag.BoolVar(&selfcheckParam, "selfcheck", true, "Enbale the self-check mode to check the vip which from config yaml instead of tcp request.")
  flag.BoolVar(&selfcheckParam, "k", true, "Enbale the self-check mode to check the vip which from config yaml. (shorten)")
  flag.Parse()

  log.SetPrefix("[gcp-healthcheck-helper] ")
  log.SetFlags(log.LstdFlags|log.Lshortfile)

  if syslogParam {
    logger, err := syslog.New(syslog.LOG_ERR|syslog.LOG_DAEMON, "[gcp-healthcheck-helper] ")
    if err == nil {
      log.SetOutput(logger)
    } else {
      log.Println(err)
    }
  }
}

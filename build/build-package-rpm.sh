#!/bin/sh

# get the script path
startdir=$(dirname "$(readlink -f $0)")
cd "${startdir}"

# check the distribution information
dist=$(cat /etc/*release* | grep '^ID=' | awk -F= '{print $2}' | sed "s/\"//g")
distver=$(cat /etc/*release* | grep 'VERSION_ID' | awk -F= '{print $2}' | sed "s/\"//g")
if [ "${dist}" != 'centos' ] && [ "${dist}" != 'redhat' ]
then
    echo 'This distribution of linux is NOT supported.'
    exit 1
elif [ "${distver}" != '7' ] && [ "${distver}" != '8' ]
then
    echo 'This distribution version is NOT supported.'
    exit 1
else
    if [ "${distver}" = '8' ]
    then
        PKGCMD='dnf'
    else
        PKGCMD='yum'
    fi
fi

# variables
pkg_name='gcp-healthcheck-helper'
pkg_version='1.0.0'
build_date=$(date +%F)
go_version='1.15.8'
export DESTDIR='/tmp/gcp-healthcheck-helper'

# install dependancies for build
export DEBIAN_FRONTEND=noninteractive
echo 'Install the development tools group, and this will take a while...'
sudo ${PKGCMD} -y --nogpgcheck groups install 'Development Tools' > /dev/null
echo 'Install the dependancies for build, and this will take a while...'
sudo ${PKGCMD} -y install centos-release-scl
sudo ${PKGCMD} --enablerepo=centos-sclo-rh -y install rh-ruby23 rh-ruby23-ruby-devel rubygems gcc make rpm-build debbuild perl-Locale-gettext git curl wget  > /dev/null
echo 'Install the fpm via gem...'
sudo scl enable rh-ruby23 'gem install --no-document fpm'

# install the specific version go lang binary
echo 'Download the golang binary archive...'
curl -O "https://dl.google.com/go/go${go_version}.linux-amd64.tar.gz" --output go${go_version}.linux-amd64.tar.gz
echo 'Uncompress the golang binary archive...'
tar zxf "go${go_version}.linux-amd64.tar.gz" --directory=/tmp
sudo mv /tmp/go /usr/local/
sudo chown -R root:root /usr/local/go
sed -i '/GOPATH=/d' ~/.profile  # remove the existing env GOPATH in the $HOME/.profile if it exists
sed -i '/GOPATH\//d' ~/.profile # remove the existing env PATH which for GOPATH setting in the $HOME/.profile if it exists
echo 'export GOPATH=$HOME/go' | tee -a ~/.profile > /dev/null
echo 'export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin' | tee -a ~/.profile > /dev/null
. ~/.profile
rm "go${go_version}.linux-amd64.tar.gz"

# build the package
cd ..
srcdir=$(pwd)
if [ -d "${DESTDIR}" ]
then
    echo "The ${DESTDIR} directory already exists, please remove the ${DESTDIR} and try again..."
    exit 1
fi

echo "Build the ${pkg_name} package..."
cd "${srcdir}"
go build -ldflags "-X main.Version=${pkg_version} -X main.Build=${build_date}"

echo "Create the fakeroot enviroment in the ${DESTDIR}..."
mkdir -p "${DESTDIR}/etc/gcp-healthcheck-helper"
mkdir -p "${DESTDIR}/etc/default"
mkdir -p "${DESTDIR}/usr/bin"
mkdir -p "${DESTDIR}/usr/lib/systemd/system"

echo "Install the files to the ${DESTDIR}..."
install -D -m 644 "${srcdir}/config.yaml" "${DESTDIR}/etc/gcp-healthcheck-helper/config.yaml"
install -D -m 755 "${srcdir}/gcp-healthcheck-helper" "${DESTDIR}/usr/bin/gcp-healthcheck-helper"
install -D -m 644 "${startdir}/gcp-healthcheck-helper.default" "${DESTDIR}/etc/default/gcp-healthcheck-helper"
install -D -m 644 "${startdir}/gcp-healthcheck-helper.service" "${DESTDIR}/usr/lib/systemd/system/gcp-healthcheck-helper.service"

# create the deb package by fpm
echo "Create the rpm package for the ${pkg_name} by the fpm..."
cd ${startdir}
scl enable rh-ruby23 "fpm -s dir -t rpm -n \"${pkg_name}\" -v \"${pkg_version}\" -C \"${DESTDIR}\" \
  --license 'MIT' \
  --url 'https://github.com/vertspirit/gcp-healthcheck-helper' \
  --description 'A healthcheck helper to check virtual ip address of linux cluster for gcp load balancer.' \
  --maintainer 'Dongjun Wu <ziyawu@gmail.com>' \
  --after-install \"${startdir}/gcp-healthcheck-helper_post.sh\" \
  --after-remove \"${startdir}/gcp-healthcheck-helper_postrun.sh\""

# clean up
echo "Remove the ${DESTDIR} directory..."
rm -rf "${DESTDIR}"
sudo ${PKGCMD} -y clean all

echo 'Done.'
